"use strict";

/*
При завантаженні сторінки міняти дату в хедері 
на поточну у форматі день місяць, рік. 
Місяць обов'язково словом (січень, лютий тощо).
Встановити розмір шрифту в 24px та жирність bold.

00 Month, 1990

"січень, лютий, березень, квітень, травень, червень, липень, серпень, вересень, жовтень, листопад, грудень"
*/

const months = "січень, лютий, березень, квітень, травень, червень, липень, серпень, вересень, жовтень, листопад, грудень";


const date = new Date();
console.log(date);

const dateStr = `${date.getDate()} ${months.split(", ")[date.getMonth()]}, ${date.getFullYear()}`;
console.log(dateStr);

date.getDate();

// const arr = months.split(", ");
// const monthIndex = date.getMonth();
// const monthResult = arr[monthIndex];

// console.log(months.split(", ")[date.getMonth()]);

document.querySelector("#date").innerText = dateStr;

/*
Додати до всіх зображень з галереї собак alt. 
Текст брати з 'data-атрибуту'.

Видалити всі зображення собак із порожнім атрибутом src
Всім собакам з data-аттрибутом reserved змінити дизайн
Всім собакам з data-аттрибутом reserved змінити дизайн
*/

// const dogGalleryContainer = document.querySelector('.dog-gallery')
// const imgs = dogGalleryContainer.querySelectorAll('img');

const imgs = document.querySelectorAll('.dog-gallery > img');

imgs.forEach((element) => {
	element.alt = element.dataset.alt;
	element.title = element.dataset.alt;
	if(!element.getAttribute("src")){
		element.remove();
	}
	if(element.dataset.reseved === "true"){
		const divElement = document.createElement("div");
		divElement.classList.add("reservedImage");

		divElement.append(element.cloneNode(true));
		element.replaceWith(divElement);
	}
});


/* 

1. create div
2. add class
3. insert in idv -> img (clone)
4. replace img with new div


*/
