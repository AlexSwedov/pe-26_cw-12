// const list = document.querySelector('.js-list');

// // const div = document.createElement('div')

// const fragment = new DocumentFragment();

// // skhdfljhsdjfhsjkdf

// const li1 = document.createElement('li');
// li1.innerText = 'Hello';
// fragment.append(li1);

// // sjkdfhks hkdfjhsdjkf
// const li2 = document.createElement('li');
// li2.innerText = 'Second Hello';

// fragment.append(li2);

// // sljdhfl jshdf sdfsdf
// const li3 = document.createElement('li');
// li3.innerText = 'Third li';
// fragment.append(li3);

// console.log(fragment)
// // finished...

// list.append(fragment);


// const template = document.querySelector('#test-template');

// const card = template.content.cloneNode(true);

// const p = card.querySelector('p');
// const img = card.querySelector('img');

// p.innerText = 'Hello from template';
// img.src = 'https://lumiere-a.akamaihd.net/v1/images/luke-skywalker-1_5f370d5f.jpeg?region=233%2C0%2C342%2C341';

// document.querySelector('.test-container').append(card)

const persons = [
	{
		id: 1,
		"birth-year": "19BBY",
		"eye-color": "blue",
		gender: "male",
		"hair-color": "blond",
		height: 172,
		mass: 77.0,
		name: "Luke Skywalker",
		"skin-color": "fair",
		photo: 'https://lumiere-a.akamaihd.net/v1/images/luke-skywalker-1_5f370d5f.jpeg?region=233%2C0%2C342%2C341',
	},
	{
		id: 2,
		"birth-year": "112BBY",
		"eye-color": "yellow",
		height: 167,
		mass: 75.0,
		name: "C-3PO",
		"skin-color": "gold",
		photo: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTwrbf3FQrHDybuaKyeOZwnY484GJ6mGPC4Fg&usqp=CAU',
	},
	{
		id: 3,
		"birth-year": "33BBY",
		"eye-color": "red",
		height: 96,
		mass: 32.0,
		name: "R2-D2",
		"skin-color": "white, blue",
		photo: 'https://static.turbosquid.com/Preview/2017/07/20__13_33_08/R2D23dsmodel01.jpgF7CB355D-7B4A-4F39-AF13-DBD5BF6F84CDLarge.jpg'
	},
	{
		id: 4,
		"birth-year": "41.9BBY",
		"eye-color": "yellow",
		gender: "male",
		height: 202,
		mass: 136.0,
		name: "Darth Vader",
		"skin-color": "white",
		photo: 'https://a1cf74336522e87f135f-2f21ace9a6cf0052456644b80fa06d4f.ssl.cf2.rackcdn.com/images/characters/large/800/Darth-Vader.Star-Wars-Series.webp'
	},
	{
		id: 5,
		"birth-year": "19BBY",
		"eye-color": "brown",
		gender: "female",
		"hair-color": "brown",
		height: 150,
		mass: 49.0,
		name: "Leia Organa",
		"skin-color": "light",
		photo: 'https://external-preview.redd.it/4hpnprC9k6vbgDz8kmRH0DunbgdxPfkaadpycrMQVMM.jpg?auto=webp&s=e084b72137f26b5343c9d9f7e8f9a5d31ccf5c3a'
	},
];


/*
Малювати на екран картки героїв фільму "Зоряні війни". На картці має бути аватарка героя - верхній блок розтягнутий по всій ширині висотою 400px, заголовок з ім'ям героя, маркований список всіх характеристик героя.
![](assets/SW.jpg)
*/

/* 
	<div id="" class="sw-gallery__item">
		<div class="sw-gallery__item-img">
			<img src="" alt="">
		</div>
		<h3>Leia Organa</h3>
		<ul>
			<li>birth-year: 19BBY</li>
			<li>eye-color": "brown"</li>
			<li>gender: "female"</li>
			<li>hair-color: brown</li>
			<li>height: 150,</li>
			<li>mass: 49.0,</li>
			<li>skin-color: light</li>
		</ul>
	</div>
*/

const gallery = document.querySelector('.sw-gallery');

const cardArr = persons.map((element) => {
	const arrElementKeys = Object.keys(element);
	const arrElementValues = Object.values(element);
	let imgSrc = '';
	let name = '';
	let id = '';
	const properties = [];

	arrElementKeys.forEach((el, i) => {
		switch (el) {
			case 'photo':
				imgSrc = arrElementValues[i];
				break;
			case 'name':
				name = arrElementValues[i];
				break;
			case 'id':
				id = arrElementValues[i];
				break;
			default:
				const upperCaseEl = el.toLocaleUpperCase();
				properties.push(`${upperCaseEl}: ${arrElementValues[i]}`);
				break;
		}
	});

	const list = properties.map((str) => {
		const listItem = str;
		return `<li>${listItem}</li>`;
	});

	return `
	<div id="${id}" class="sw-gallery__item">
		<div class="sw-gallery__item-img">
			<img src="${imgSrc}" alt="">
		</div>
		<h3>${name}</h3>
		<ul>
			${list.join('')}
		</ul>
	</div>`;
});

gallery.insertAdjacentHTML('beforeend', cardArr.join(''));